var modal = document.getElementById('simple-modal');
var modalBtns = document.querySelectorAll('modalBtn');
var closeBtn = document.getElementById('closeBtn');
var modalimg = document.getElementById('modal-ig');
var imgsrc
// open the modal
for(var i=0, len = modalBtns.length; i < len; i++){
    var obj = this;
    modalBtns[i].addEventListener('click', openModal(obj));
}


closeBtn.addEventListener('click', closeModal);

function closeModal() {
  modal.style.display = 'none';
}
function openModal(obj) {
  imgsrc = obj.src;
  // console.log(obj);
  modal.style.display = 'block';
  modalimg.src = imgsrc;
}

function scrollBehavior() {
// 'use strict';

  var section = document.querySelectorAll(".section");
  var sections = {};
  var i = 0;

  Array.prototype.forEach.call(section, function(e) {
  sections[e.id] = e.offsetTop;
  });
    window.onscroll = function() {
    // console.log(document.getScroll());
      Array.prototype.forEach.call(section, function(e) {
    // get the height of each section element
        sections[e.id] = e.offsetTop;
    // console.log(e.documentElement.scrollTop);
      });

      if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
      // resize the navbar
        document.getElementById("mainNav").style.padding = "10px 0px";
      } else {
      // original size of navbar
        document.getElementById("mainNav").style.padding = "20px 0px";
      }
      var scrollPosition = document.documentElement.scrollTop || document.body.scrollTop;
      for (i in sections) {
        if (sections[i] <= scrollPosition+300) {
          document.querySelector(".nav-right" + ".active").setAttribute("class", "nav-right");
          document.querySelector('a[datatarget*=' + i + ']').setAttribute("class","nav-right active");
        }
      }
  };
}

scrollBehavior();
